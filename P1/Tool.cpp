#include "stdafx.h"
//Line
Tool_Line::Tool_Line(ChildWindowData *r) {
	childWndData = r;
}

void Tool_Line::OnMouseDown(HWND hWnd, int left, int top, int right, int bottom) {
	Line_O *line = new Line_O(left, top, right, bottom, childWndData->color);
	childWndData->AddObjectToList(line);
}

void Tool_Line::OnMouseMove(int x, int y) {
	Object* obj = childWndData->GetLastObject();
	if (obj != NULL)
		obj->MoveHandleTo(x, y);
}

void Tool_Line::OnMouseUp(int left, int top, int right, int bottom) {

}

void Tool_Line::GetText(WCHAR src[100])
{
	return;
}

// Rectangle
Tool_Rectangle::Tool_Rectangle(ChildWindowData *r) {
	childWndData = r;
}

void Tool_Rectangle::OnMouseDown(HWND hWnd, int left, int top, int right, int bottom) {
	Rectangle_O *rect = new Rectangle_O(left, top, right, bottom, childWndData->color);
	childWndData->AddObjectToList(rect);
}

void Tool_Rectangle::OnMouseMove(int x, int y) {
	Object* obj = childWndData->GetLastObject();
	if (obj != NULL)
		obj->MoveHandleTo(x, y);
}

void Tool_Rectangle::OnMouseUp(int left, int top, int right, int bottom) {

}

void Tool_Rectangle::GetText(WCHAR src[100])
{
	return;
}

//Ellipse
Tool_Ellipse::Tool_Ellipse(ChildWindowData *r) {
	childWndData = r;
}

void Tool_Ellipse::OnMouseDown(HWND hWnd, int left, int top, int right, int bottom) {
	Ellipse_O *ellipse = new Ellipse_O(left, top, right, bottom, childWndData->color);
	childWndData->AddObjectToList(ellipse);
}

void Tool_Ellipse::OnMouseMove(int x, int y) {
	Object* obj = childWndData->GetLastObject();
	if (obj != NULL)
		obj->MoveHandleTo(x, y);
}

void Tool_Ellipse::OnMouseUp(int left, int top, int right, int bottom) {

}

void Tool_Ellipse::GetText(WCHAR src[100])
{
	return;
}

//Text
Tool_Text::Tool_Text(ChildWindowData *r) {
	childWndData = r;
}

void Tool_Text::OnMouseDown(HWND hWnd, int left, int top, int right, int bottom) {
	Text_O *text = new Text_O(left, top, right, bottom, childWndData->color, childWndData->font);
	swprintf_s(text->str, 100, tmp_text);
	childWndData->AddObjectToList(text);
}

void Tool_Text::GetText(WCHAR src[100]) {
	swprintf_s(tmp_text, 100, src);
}

void Tool_Text::OnMouseMove(int x, int y) {
	//Object* obj = childWndData->GetLastObject();
	//if (obj != NULL)
	//	obj->MoveHandleTo(x, y);
}


void Tool_Text::OnMouseUp(int left, int top, int right, int bottom) {

}

//Select_Object
Tool_Select_Object::Tool_Select_Object(ChildWindowData *r) {
	childWndData = r;
	isSelected = false;
}

void Tool_Select_Object::OnMouseDown(HWND hWnd, int left, int top, int right, int bottom) {
	if (isSelected == true) {
		RECT hRect;
		GetClientRect(hWnd, &hRect);
		InvalidateRect(hWnd, &hRect, false);
		isSelected = false;
		childWndData->selectedObjIndex = -1;
		return;
	}
	int i = -1;
	for (std::list<Object*>::reverse_iterator rit = childWndData->objects->rbegin(); rit != childWndData->objects->rend(); ++rit) {
		i++;
		childWndData->selectedObjIndex = i;
		//Kiem tra duoc chon
		if ((*rit)->IsSelected(left, top)) {
			isSelected = true;
			//Ve ra 4 dau cham
			(*rit)->Selected(hWnd);
			return;
		}
		if (rit == childWndData->objects->rend())
			childWndData->selectedObjIndex = -1;
	}
}

void Tool_Select_Object::OnMouseMove(int x, int y) {
	
}

void Tool_Select_Object::OnMouseUp(int left, int top, int right, int bottom) {

}

void Tool_Select_Object::GetText(WCHAR src[100])
{
	return;
}
