#pragma once
#ifndef _TOOL_H_
#define _TOOL_H_

#include "stdafx.h"
#include "Resource.h"

class ChildWindowData;

class Tool {
public:
	bool	isSelected;
	ChildWindowData * childWndData;
	virtual void OnMouseDown(HWND hWnd, int left, int top, int right, int bottom) = 0;
	virtual void OnMouseMove(int x, int y) = 0;
	virtual void OnMouseUp(int left, int top, int right, int bottom) = 0;
	virtual void GetText(WCHAR src[100]) = 0;
};

class Tool_Line : public Tool {
public:
	Tool_Line(ChildWindowData *r);

	void OnMouseDown(HWND hWnd, int left, int top, int right, int bottom);
	void OnMouseMove(int x, int y);
	void OnMouseUp(int left, int top, int right, int bottom);
	void GetText(WCHAR src[100]);
};

class Tool_Rectangle : public Tool {
public:
	Tool_Rectangle(ChildWindowData *r);

	void OnMouseDown(HWND hWnd, int left, int top, int right, int bottom);
	void OnMouseMove(int x, int y);
	void OnMouseUp(int left, int top, int right, int bottom);
	void GetText(WCHAR src[100]);
};

class Tool_Ellipse : public Tool {
public:
	Tool_Ellipse(ChildWindowData *r);

	void OnMouseDown(HWND hWnd, int left, int top, int right, int bottom);
	void OnMouseMove(int x, int y);
	void OnMouseUp(int left, int top, int right, int bottom);
	void GetText(WCHAR src[100]);
};

class Tool_Text : public Tool {
public:
	WCHAR tmp_text[100];
	Tool_Text(ChildWindowData *r);

	void OnMouseDown(HWND hWnd, int left, int top, int right, int bottom);
	void GetText(WCHAR src[100]);
	void OnMouseMove(int x, int y);
	void OnMouseUp(int left, int top, int right, int bottom);
};

class Tool_Select_Object : public Tool {
public:
	Tool_Select_Object(ChildWindowData *r);

	void OnMouseDown(HWND hWnd, int left, int top, int right, int bottom);
	void OnMouseMove(int x, int y);
	void OnMouseUp(int left, int top, int right, int bottom);
	void GetText(WCHAR src[100]);

	
};

#endif