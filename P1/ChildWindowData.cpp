#include "stdafx.h"

ChildWindowData::ChildWindowData() {
	activeTool = ToolType::Line;
	objects = new std::list<Object*>();
	tools = new std::vector<Tool*>();
	tools->push_back(new Tool_Line(this));
	tools->push_back(new Tool_Rectangle(this));
	tools->push_back(new Tool_Ellipse(this));
	tools->push_back(new Tool_Text(this));
	tools->push_back(new Tool_Select_Object(this));
}

ChildWindowData::~ChildWindowData() {
	//Delete objects
	for (std::list<Object*>::iterator it = objects->begin(); it != objects->end(); ++it)
	{
		delete *it;
	}
	delete objects;

	//Delete tools
	for (int i = 0; i < (int)tools->size(); ++i)
		delete tools->at(i);
	delete tools;
}

void ChildWindowData::AddObjectToList(Object *obj) {
	objects->push_front(obj);
}

Object* ChildWindowData::GetLastObject() {
	std::list<Object*>::iterator it = objects->begin();
	if (it == objects->end())
		return NULL;

	return (*it);
}

Tool* ChildWindowData::GetCurrentTool() {
	return tools->at(int(activeTool));
}

void ChildWindowData::Draw(HDC hdc) {
	for (std::list<Object*>::reverse_iterator rit = objects->rbegin(); rit != objects->rend(); ++rit) {
		(*rit)->Draw(hdc);
	}
}

void ChildWindowData::GetColor(COLORREF _color) {
	color = _color;
}

void ChildWindowData::GetFont(LOGFONT _font) {
	font = _font;
}

void ChildWindowData::GetFilename(TCHAR _filename[]) {
	swprintf_s(filename, 256, _filename);
}

void ChildWindowData::SaveFile() {
	fstream file;
	wstring appendedfilename;
	appendedfilename += filename;
	appendedfilename += L".drw";
	LPCWSTR tmp = appendedfilename.c_str();

	file.open(appendedfilename, ios::out);
	if (file.is_open()) {
		file << (int)objects->size() << endl;
		for (std::list<Object*>::iterator it = objects->begin(); it != objects->end(); ++it)
		{
			file << (int)(*it)->type << '\n';

			file << (*it)->left << " ";
			file << (*it)->top << " ";
			file << (*it)->right << " ";
			file << (*it)->bottom << '\n';

			file << (int)GetRValue((*it)->color) << " ";
			file << (int)GetGValue((*it)->color) << " ";
			file << (int)GetBValue((*it)->color) << '\n';

			if ((int)(*it)->type == 3) {
				for (int i = 0; i < 32; i++) {
					//lfFacetime[32]
					if (checkCharacterIsFont((CHAR)(*it)->font.lfFaceName[i]))
						file << (CHAR)(*it)->font.lfFaceName[i];
				}
				file << '\n';
				file << (long)(*it)->font.lfHeight << " ";
				file << (long)(*it)->font.lfWeight << " ";
				if ((*it)->font.lfItalic) file << "1" << " ";
				else file << "0" << " ";
				if ((*it)->font.lfUnderline) file << "1" << "\n";
				else file << "0" << '\n';
				for (int i = 0; i < 100; i++) {
					//Text[100]
					if (checkCharacterIsStr((CHAR)(*it)->str[i]))
						file << (CHAR)(*it)->str[i];
				}
				file << "\n";
			}
			else {
				file << "0\n0 0 0 0 \n0\n";
			}
		}

		//Get file title
		//Spilt path and filename. Ex: C:\\Dekstop\\123.drw => 123.drw
		int count = 0;
		for (int i = wcslen(tmp); i >= 4; i--) {
			if (tmp[i] == '\\') {
				count = i + 1;
				break;
			}
		}
		wstring fileTitle;
		for (int i = count; i <= wcslen(tmp); i++) {
			fileTitle += tmp[i];
		}
		LPCWSTR tmp2 = fileTitle.c_str();
		//Set file title
		Controller::GetInstance()->SetWindowTitle(tmp2);
	}
	else {
		cout << "Can't create " << filename << " on disk" << endl;
		return;
	}

	file.close();
}


bool ChildWindowData::checkCharacterIsFont(CHAR x) {
	if (x == 32 || x >= 65 && x <= 90 || x >= 97 && x <= 122)
		return true;
	else
		return false;
}

bool ChildWindowData::checkCharacterIsStr(CHAR x) {
	if (x >= 20 && x <= 126)
		return true;
	else
		return false;
}

void ChildWindowData::OpenFile() {
	fstream file;
	file.open(filename, ios::in);
	if (file.is_open()) {
		int n;
		file >> n; //Number of items;
		if (n > 0) {
			ChildWindowData* childWndData = Controller::GetInstance()->GetActiveWndData();
			for (int i = 0; i < n; i++) {
				int type, left, top, right, bottom, r, g, b, tmp; //Tmp to store trash
				COLORREF file_color;
				file >> type;
				file >> left >> top >> right >> bottom;
				file >> r >> g >> b;
				file_color = RGB(r, g, b);
				switch (type) {
				case 0: {
					Line_O * line = new Line_O(left, top, right, bottom, file_color);
					childWndData->AddObjectToList(line);
					int tmp;
					file >> tmp >> tmp >> tmp >> tmp >> tmp >> tmp;
				}
					break;
				case 1: {
					Rectangle_O *rect = new Rectangle_O(left, top, right, bottom, file_color);
					childWndData->AddObjectToList(rect);
					file >> tmp >> tmp >> tmp >> tmp >> tmp >> tmp;
				}
					break;
				case 2: {
					Ellipse_O *ellipse = new Ellipse_O(left, top, right, bottom, file_color);
					childWndData->AddObjectToList(ellipse);
					file >> tmp >> tmp >> tmp >> tmp >> tmp >> tmp;
				}
					break;
				case 3: {
					/*LOGFONT  lf;
					string str;
					wcscpy_s(lf.lfFaceName, LF_FACESIZE, L"Arial");
					file >> lf.lfHeight;
					file >> lf.lfWeight;
					file >> lf.lfItalic;
					file >> lf.lfUnderline;
					getline(file, str);
					Text_O *text = new Text_O(left, top, right, bottom, file_color, lf);
					wstring wide_string = wstring(str.begin(), str.end());
					const wchar_t* result_txt = wide_string.c_str();
					swprintf_s(text->str, 100, result_txt);
					childWndData->AddObjectToList(text);*/
				}
					break;
				}
			}

			//Get file title
			//Spilt path and filename. Ex: C:\\Dekstop\\123.drw => 123.drw
			int count = 0;
			for (int i = wcslen(tmp); i >= 4; i--) {
				if (tmp[i] == '\\') {
					count = i + 1;
					break;
				}
			}
			wstring fileTitle;
			for (int i = count; i <= wcslen(tmp); i++) {
				fileTitle += tmp[i];
			}
			LPCWSTR tmp2 = fileTitle.c_str();
			//Set file title
			//Controller::GetInstance()->SetWindowTitle(tmp2);
		}
	}
	else {
		cout << "Can't create " << filename << " on disk" << endl;
		return;
	}
	file.close();
}

Object* ChildWindowData::GetSelectedObject() {
	if (selectedObjIndex == -1)
		return NULL;

	int i = 0;
	for (std::list<Object*>::reverse_iterator rit = objects->rbegin(); rit != objects->rend(); ++rit) {
		if (i == selectedObjIndex) {
			return (*rit);
			selectedObjIndex = -1;
		}
		else
			i++;
	}
	return NULL;
}

void ChildWindowData::Copy(HWND hWnd) {
	WCHAR szObjectFormat[20] = L"MYOBJECT";
	int nObjFormatID = RegisterClipboardFormat(szObjectFormat);
	Object *c = GetSelectedObject();
	HGLOBAL hgbMem;
	HGLOBAL hgbMemText;

	if (c != NULL) {
		OpenClipboard(NULL);
		EmptyClipboard();
		hgbMem = GlobalAlloc(NULL, sizeof(Object));
		Object *p = (Object*)GlobalLock(hgbMem);

		//Problem!!!
		p->type = c->type;
		p->left = c->left;
		p->top = c->top;
		p->right = c->right;
		p->bottom = c->bottom;
		p->color = c->color;
		p->font = c->font;
		for (int i = 0; i < 100; i++) {
			p->str[i] = c->str[i];
		}

		SetClipboardData(nObjFormatID, hgbMem);
		GlobalUnlock(hgbMem);
		CloseClipboard();
	} 


	//if (c != NULL) {
	//	OpenClipboard(NULL);
	//	EmptyClipboard();
	//	if (c->type == ToolType::Line) {
	//		hgbMem = GlobalAlloc(NULL, sizeof(Line_O));
	//		Line_O *p = (Line_O*)GlobalLock(hgbMem);
	//		p->type = c->type;
	//		p->left = c->left;
	//		p->top = c->top;
	//		p->right = c->right;
	//		p->bottom = c->bottom;
	//		p->color = c->color;
	//		p->font = c->font;
	//		wcscpy_s(p->str, sizeof(c->str), c->str);

	//		SetClipboardData(nObjFormatID, hgbMem);
	//		GlobalUnlock(hgbMem);
	//		CloseClipboard();
	//	}
	//	else if (c->type == ToolType::Rectangle) {
	//		hgbMem = GlobalAlloc(NULL, sizeof(Rectangle_O));
	//		Rectangle_O *p = (Rectangle_O*)GlobalLock(hgbMem);
	//		p->type = c->type;
	//		p->left = c->left;
	//		p->top = c->top;
	//		p->right = c->right;
	//		p->bottom = c->bottom;
	//		p->color = c->color;
	//		p->font = c->font;
	//		wcscpy_s(p->str, sizeof(c->str), c->str);

	//		SetClipboardData(nObjFormatID, hgbMem);
	//		GlobalUnlock(hgbMem);
	//		CloseClipboard();
	//	}
	//	else if (c->type == ToolType::Ellipse) {
	//		hgbMem = GlobalAlloc(NULL, sizeof(Ellipse_O));
	//		Ellipse_O *p = (Ellipse_O*)GlobalLock(hgbMem);
	//		p->type = c->type;
	//		p->left = c->left;
	//		p->top = c->top;
	//		p->right = c->right;
	//		p->bottom = c->bottom;
	//		p->color = c->color;
	//		p->font = c->font;
	//		wcscpy_s(p->str, sizeof(c->str), c->str);

	//		SetClipboardData(nObjFormatID, hgbMem);
	//		GlobalUnlock(hgbMem);
	//		CloseClipboard();
	//	}
	//	else if (c->type == ToolType::Text) {
	//		hgbMem = GlobalAlloc(NULL, sizeof(Text_O));
	//		Text_O *p = (Text_O*)GlobalLock(hgbMem);
	//		p->type = c->type;
	//		p->left = c->left;
	//		p->top = c->top;
	//		p->right = c->right;
	//		p->bottom = c->bottom;
	//		p->color = c->color;
	//		p->font = c->font;
	//		wcscpy_s(p->str, sizeof(c->str), c->str);

	//		SetClipboardData(nObjFormatID, hgbMem);
	//		GlobalUnlock(hgbMem);
	//		CloseClipboard();
	//	}

	//	CloseClipboard();
		

		/*GlobalUnlock(hgbMem);
		if (OpenClipboard(hWnd))
		{
			EmptyClipboard();
			SetClipboardData(nObjFormatID, hgbMem);
			
		}
	}*/
	else {
		MessageBox(hWnd, L"OBJECT NOT FOUND", L"ERROR", MB_OK);
	}
}

void ChildWindowData::Paste(HWND hWnd) {
	if (OpenClipboard(NULL))
	{
		WCHAR szObjectFormat[20] = L"MYOBJECT";
		int nObjFormatID = RegisterClipboardFormat(szObjectFormat);
		HGLOBAL hgbMem = GetClipboardData(nObjFormatID);
		HGLOBAL hgbMemText = GetClipboardData(CF_TEXT);
		if (hgbMem)
		{
			Object *p = (Object*)GlobalLock(hgbMem);

			if (p->type == ToolType::Line) {
				Line_O *c = new Line_O(p->left, p->top, p->right, p->bottom, p->color);
				AddObjectToList(c);
			}
			else if (p->type == ToolType::Rectangle) {
				Rectangle_O *c = new Rectangle_O(p->left, p->top, p->right, p->bottom, p->color);
				AddObjectToList(c);
			}
			else if (p->type == ToolType::Ellipse) {
				Ellipse_O *c = new Ellipse_O(p->left, p->top, p->right, p->bottom, p->color);
				AddObjectToList(c);
			}
			else if (p->type == ToolType::Text) {
				Text_O *c = new Text_O(p->left, p->top, p->right, p->bottom, p->color, p->font);
				for (int i = 0; i < 100; i++) {
					c->str[i] = p->str[i];
				}
				AddObjectToList(c);
			}

			//reDraw
			hWnd = Controller::GetInstance()->hActiveChildWnd;
			Draw(GetDC(hWnd));

			GlobalUnlock(hgbMem);
			//EmptyClipboard();
			CloseClipboard();
		}
		else if (hgbMemText) {
			char *szText;
			char *pData = (char*)GlobalLock(hgbMemText);
			szText = new char[strlen(pData) + 1];
			strcpy_s(szText, 100, pData);
			GlobalUnlock(hgbMemText);
			CloseClipboard();

			//From char to WCHAR
			const WCHAR *pwcsName;
			// required size
			int nChars = MultiByteToWideChar(CP_ACP, 0, szText, -1, NULL, 0);
			pwcsName = new WCHAR[nChars];
			MultiByteToWideChar(CP_ACP, 0, szText, -1, (LPWSTR)pwcsName, nChars);

			// use it....
			Text_O *text = new Text_O(0, 0, 0, 0, color, font);
			for (int i = 0; i < 100; i++) {
				text->str[i] = pwcsName[i];
			}
			//swprintf_s(text->str, 100, pwcsName);
			AddObjectToList(text);

			// delete it
			delete[] pwcsName;
			//reDraw
			hWnd = Controller::GetInstance()->hActiveChildWnd;
			Draw(GetDC(hWnd));
		} else {
			MessageBox(hWnd, L"OBJECT NOT FOUND", L"ERROR", MB_OK);
			CloseClipboard();
		}
	}
}

void ChildWindowData::Delete(HWND hWnd) {
	Object *c = GetSelectedObject();
	objects->remove(c);

	hWnd = Controller::GetInstance()->hActiveChildWnd;
	activeTool = ToolType::Select_Object;
	GetCurrentTool()->OnMouseDown(hWnd, 0, 0, 0, 0);	
	Draw(GetDC(hWnd));
}

void ChildWindowData::Cut(HWND hWnd) {
	hWnd = Controller::GetInstance()->hActiveChildWnd;
	Copy(hWnd);
	Delete(hWnd);
}