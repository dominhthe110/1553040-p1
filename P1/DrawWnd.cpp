#include "stdafx.h"


ATOM MyRegisterDrawClass(HINSTANCE hInstance) {
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = DrawWndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 8;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_P1));
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = L"MDI_DRAW_CHILD";
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	if (!RegisterClassEx(&wcex)) {
		MessageBox(0, L"Can't register Draw class", L"Oh oh", MB_OK);
		return FALSE;
	}
	return RegisterClassEx(&wcex);
}

LRESULT CALLBACK DrawWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	wmId = LOWORD(wParam);
	wmEvent = HIWORD(wParam);
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
	case WM_COMMAND:
		wmId = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{

		default:
			return DefMDIChildProc(hWnd, message, wParam, lParam);
		}
		break;

	case WM_CREATE: /*{
		ChildWindowData * objdata = (ChildWindowData*)VirtualAlloc(NULL, sizeof(ChildWindowData), MEM_COMMIT, PAGE_READWRITE);
		SetWindowLongPtr(hWnd, 0, (LONG_PTR)objdata);
		ChildWindowData* objdata1 = (ChildWindowData*)GetWindowLongPtr(hWnd, 0);
	}*/
		InitDrawWnd(hWnd);
		break;

	case WM_MDIACTIVATE: 
		Controller::GetInstance()->hActiveChildWnd = hWnd;
		break;

	case WM_LBUTTONDOWN:
		OnLButtonDown(hWnd, wParam, lParam);
		break;

	case WM_MOUSEMOVE:
		OnMouseMove(hWnd, wParam, lParam);
		break;

	case WM_SIZE:
		break;

	case WM_PAINT: {
		hdc = BeginPaint(hWnd, &ps);

		//Client region
		RECT r;
		GetClientRect(hWnd, &r);
		int nWidth = r.right - r.left;
		int nHeight = r.bottom - r.top;

		HDC hMemDC = CreateCompatibleDC(hdc);
		HBITMAP hBmp = CreateCompatibleBitmap(hdc, nWidth, nHeight);

		HGDIOBJ hOldBmp = SelectObject(hMemDC, hBmp);
		FillRect(hMemDC, &r, HBRUSH(GetStockObject(WHITE_BRUSH)));

		//Draw 
		ChildWindowData* windowData = (ChildWindowData*)GetWindowLongPtr(hWnd, 0);
		windowData->Draw(hMemDC);
		BitBlt(hdc, 0, 0, nWidth, nHeight, hMemDC, 0, 0, SRCCOPY);

		SelectObject(hMemDC, hOldBmp);

		DeleteObject(hBmp);
		DeleteDC(hMemDC);

		EndPaint(hWnd, &ps);
		break;
	}

	case WM_DESTROY: {
		n_draw--;
		break;
	}

	default:
		return DefMDIChildProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

LRESULT CALLBACK MDICloseProc(HWND hMDIWnd, LPARAM lParam)
{
	SendMessage(hWndDrawClient, WM_MDIDESTROY, (WPARAM)hMDIWnd, 0L);
	return 1;
}

void InitDrawWnd(HWND hWnd) {
	ChildWindowData* objdata = new ChildWindowData;
	//SetWindowLong(hWnd, 0, (LONG_PTR)objdata);
	//ChildWindowData *objdata = (ChildWindowData*)VirtualAlloc(NULL, sizeof(ChildWindowData), MEM_COMMIT, PAGE_READWRITE);
	objdata->color = RGB(0, 0, 0);
	ZeroMemory(&(objdata->font), sizeof(objdata->font));
	objdata->font.lfHeight = 20;
	wcscpy_s(objdata->font.lfFaceName, LF_FACESIZE, L"Arial");
	SetLastError(0);
	if (SetWindowLongPtr(hWnd, 0, (LONG_PTR)objdata) == 0)
		if (GetLastError() != 0) {
			MessageBox(hWnd, L"Can't init data pointer of window", L"Error", MB_OK);
		}
	return;
}


void OnLButtonDown(HWND hWnd, WPARAM wParam, LPARAM lParam) {
	int x, y;
	RECT rc;
	GetClientRect(hWnd, &rc);
	
	x = LOWORD(lParam);
	y = HIWORD(lParam);
	
	//x = LOWORD(lParam) + rc.left;
	//y = HIWORD(lParam) + rc.top;

	ChildWindowData* windowData = (ChildWindowData*)GetWindowLongPtr(hWnd, 0);
	if (windowData->activeTool == ToolType::Text) {
		DialogBox(NULL, MAKEINTRESOURCE(IDD_DIALOG1), hWnd, InputTextDlg);
		windowData->GetCurrentTool()->GetText(tmp);
	}

	windowData->GetCurrentTool()->OnMouseDown(hWnd, x, y, x, y);
	if (windowData->activeTool == ToolType::Text) {
		RECT hRect;
		GetClientRect(hWnd, &hRect);
		InvalidateRect(hWnd, &hRect, false);
	}
}

void OnMouseMove(HWND hWnd, WPARAM wParam, LPARAM lParam) {
	if (!(wParam & MK_LBUTTON))
		return;
	
	int x, y;
	RECT rc;
	GetClientRect(hWnd, &rc);
	x = LOWORD(lParam);
	y = HIWORD(lParam);
	//x = LOWORD(lParam) + rc.left;
	//y = HIWORD(lParam) + rc.top;

	ChildWindowData* windowData = (ChildWindowData*)GetWindowLongPtr(hWnd, 0);
	windowData->GetCurrentTool()->OnMouseMove(x, y);

	RECT hRect;
	GetClientRect(hWnd, &hRect);
	InvalidateRect(hWnd, &hRect, false);
}

INT_PTR CALLBACK InputTextDlg(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG: {
		EnableWindow(GetDlgItem(hDlg, IDOK), FALSE);
		SetFocus(GetDlgItem(hDlg, IDC_EDIT1));

		return (INT_PTR)TRUE;
	}

	case WM_COMMAND:
		switch (LOWORD(wParam)) {

		case IDOK: {
			GetDlgItemTextW(hDlg, IDC_EDIT1, tmp, 100);
			EndDialog(hDlg, (INT_PTR)tmp);
			DestroyWindow(hDlg);
			//PostQuitMessage(0);
		}
		break;

		case IDC_EDIT1: {
			if (HIWORD(wParam) == EN_CHANGE) {
				LRESULT nTextLen = SendMessage((HWND)lParam, WM_GETTEXTLENGTH, 0, 0L);
				EnableWindow(GetDlgItem(hDlg, IDOK), (nTextLen != 0) ? TRUE : FALSE);
			}
		}
		break;

		case IDCANCEL: {
			swprintf_s(tmp, 1, L" ");
			EndDialog(hDlg, (INT_PTR)tmp);
			DestroyWindow(hDlg);
			//PostQuitMessage(0);
			//return (INT_PTR)TRUE;
		}
		break;

		}


	}
	return (INT_PTR)FALSE;
}
