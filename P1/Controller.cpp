#include "stdafx.h"

Controller::Controller() {
	hActiveChildWnd = NULL;
}

ChildWindowData* Controller::GetActiveWndData()
{
	if (hActiveChildWnd == NULL)
		return NULL;

	return (ChildWindowData*)GetWindowLongPtr(hActiveChildWnd, 0);
}

Controller* Controller::GetInstance()
{
	static Controller* instance = new Controller();

	return instance;
}

bool Controller::SetWindowTitle(LPCWSTR A) {
	return SetWindowText(hActiveChildWnd, A);
}