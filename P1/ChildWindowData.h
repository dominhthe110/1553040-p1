#pragma once
#ifndef _CHILDWINDOWDATA_H_
#define _CHILDWINDOWDATA_H_

#include "stdafx.h"
#include "Resource.h"
#include <list>
#include <vector>
#include <fstream>
#include <sstream>
using namespace std;

class ChildWindowData {
public:
	//Data
	std::list<Object*> *objects;

	//For implementation
	ToolType	activeTool;
	LOGFONT		font;
	COLORREF	color;
	std::vector<Tool*> *tools;
	TCHAR filename[256];
	unsigned int	selectedObjIndex = -1;
	ChildWindowData();
	~ChildWindowData();

	void AddObjectToList(Object *obj);
	Object* GetLastObject();
	Tool*	GetCurrentTool();
	void	Draw(HDC hdc);
	void	GetColor(COLORREF);
	void	GetFont(LOGFONT);

	//Save file
	void	GetFilename(TCHAR _filename[]);
	void	SaveFile();
	bool	checkCharacterIsFont(CHAR);
	bool	checkCharacterIsStr(CHAR);
	//Open file
	void	OpenFile();

	//P3
	Object*	GetSelectedObject();
	void	Cut(HWND hWnd);
	void	Copy(HWND hWnd);
	void	Paste(HWND hWnd);
	void	Delete(HWND hWnd);
};




#endif
