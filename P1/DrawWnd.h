#pragma once
#ifndef _DRAW_WND_H_
#define _DRAW_WND_H_

#include "stdafx.h"
#include "Resource.h"

static WCHAR	tmp[100];

ATOM				MyRegisterDrawClass(HINSTANCE hInstance);
LRESULT CALLBACK    DrawWndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK	MDICloseProc(HWND hMDIWnd, LPARAM lParam);
INT_PTR CALLBACK	InputTextDlg(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);

void				InitDrawWnd(HWND);
void				OnLButtonDown(HWND hWnd, WPARAM wParam, LPARAM lParam);
void				OnMouseMove(HWND hWnd, WPARAM wParam, LPARAM lParam);


#endif