#pragma once
#ifndef _CONTROLLER_H_
#define _CONTROLLER_H_

#include "stdafx.h"
#include "Resource.h"

class Controller
{
public:
	HINSTANCE hInst;
	HWND hActiveChildWnd;

	Controller();

	ChildWindowData *GetActiveWndData();
	static Controller *GetInstance();
	bool	SetWindowTitle(LPCWSTR A);
};

#endif