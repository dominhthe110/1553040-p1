#pragma once
#ifndef _OBJECT_H_
#define _OBJECT_H_

#include "stdafx.h"
#include "Resource.h"
#include <list>
#include <vector>
enum class ToolType {Line, Rectangle, Ellipse, Text, Select_Object};

class Object {
public:
	ToolType		type;
	int				left, top, right, bottom;
	COLORREF		color = RGB(0, 0, 0);
	LOGFONT			font;
	WCHAR			str[100];

	virtual void	Draw(HDC hdc) = 0;
	virtual void	MoveHandleTo(int x, int y) = 0;
	virtual bool	IsSelected(int x, int y) = 0;
	virtual void	Selected(HWND hWnd) = 0;
};

class Line_O : public Object {
public:
	Line_O(int _left, int _top, int _right, int _bottom, COLORREF _color);
	void	Draw(HDC hdc);
	void	MoveHandleTo(int x, int y);
	bool	IsSelected(int x, int y);
	void	Selected(HWND hWnd);
};

class Rectangle_O : public Object {
public:
	Rectangle_O(int _left, int _top, int _right, int _bottom, COLORREF _color);
	void	Draw(HDC hdc);
	void	MoveHandleTo(int x, int y);
	bool	IsSelected(int x, int y);
	void	Selected(HWND hWnd);
};

class Ellipse_O : public Object {
public:
	Ellipse_O(int _left, int _top, int _right, int _bottom, COLORREF _color);
	void	Draw(HDC hdc);
	void	MoveHandleTo(int x, int y);
	bool	IsSelected(int x, int y);
	void	Selected(HWND hWnd);
};


class Text_O : public Object {
public:
	Text_O(int _left, int _top, int _right, int _bottom, COLORREF _color, LOGFONT _font);
	void	Draw(HDC hdc);
	void	MoveHandleTo(int x, int y);
	bool	IsSelected(int x, int y);
	void	Selected(HWND hWnd);
};


#endif