// P1.cpp : Defines the entry point for the application.
// Do Minh The - 1553040
#include "stdafx.h"

#define MAX_LOADSTRING 100
#define ID_TOOLBAR		1000	// ID of the toolbar
#define IMAGE_WIDTH     18
#define IMAGE_HEIGHT    17
#define BUTTON_WIDTH    0
#define BUTTON_HEIGHT   0
#define TOOL_TIP_MAX_LEN   32


// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

HWND hWndDrawClient;
HWND hToolBarWnd;
HWND hWnd;
int n_draw = 0;
static int nCurrentSelectedMenuItem;

// Forward declarations of functions included in this code module:

// P1
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
void				CreateDrawWin();
void				UpdateStateMenu(HWND hWnd, int newMenuItem);
void				DrawChooseColor(HWND hWnd);
void				DrawChooseFont(HWND hWnd);
void				DrawSaveFile(HWND hWnd);
void				DrawOpenFile(HWND hWnd);
void				CreateDrawWinFromFile(LPCWSTR  draw_title);

void				doCreate_ToolBar(HWND hWnd);
void				doToolBar_AddUserButton();


int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_P1, szWindowClass, MAX_LOADSTRING);

    MyRegisterClass(hInstance);
	MyRegisterDrawClass(hInstance);
    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_P1));

    MSG msg;
	
    // Main message loop:
	while (GetMessage(&msg, nullptr, 0, 0))
	{
		if ((!TranslateMDISysAccel(hWndDrawClient, &msg)) &&
			(!TranslateAccelerator(hWnd, hAccelTable, &msg)))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

    return (int) msg.wParam;
}


ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_P1));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_P1);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}


LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_CREATE: {
		//Mặc định khởi động. Check vào Draw-Line
		CheckMenuItem(GetMenu(hWnd), ID_DRAW_LINE, MF_CHECKED | MF_BYCOMMAND);
		nCurrentSelectedMenuItem = ID_DRAW_LINE;

		//
		doCreate_ToolBar(hWnd);
		doToolBar_AddUserButton();
		ShowWindow(hToolBarWnd, SW_SHOW);

		//
		CLIENTCREATESTRUCT ccs;
		ccs.hWindowMenu = GetSubMenu(GetMenu(hWnd), 2);
		ccs.idFirstChild = 50001;
		hWndDrawClient = CreateWindow(L"MDICLIENT", 0,
			WS_CHILD | WS_CLIPCHILDREN | WS_VSCROLL | WS_HSCROLL,
			0, 0, 0, 0, hWnd, 0, hInst, (LPSTR)&ccs);
		ShowWindow(hWndDrawClient, SW_SHOW);

		//
		Controller::GetInstance()->hInst = hInst;
	}
		return 0;

	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);
		int wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case ID_FILE_EXIT:
			DestroyWindow(hWnd);
			break;

		case ID_FILE_NEW: 
			CreateDrawWin();
			break;

		case ID_FILE_OPEN:
			MessageBox(hWnd, L"Bạn đã chọn Open", L"Thông báo", MB_OK);
			DrawOpenFile(hWnd);
			break;

		case ID_FILE_SAVE:
			MessageBox(hWnd, L"Bạn đã chọn Save", L"Thông báo", MB_OK);
			DrawSaveFile(hWnd);
			((ChildWindowData*)Controller::GetInstance()->GetActiveWndData())->SaveFile();
			break;

		case ID_EDIT_CUT:
			if (n_draw > 0)
				((ChildWindowData*)Controller::GetInstance()->GetActiveWndData())->Cut(hWnd);
			break;

		case ID_EDIT_COPY:
			if (n_draw > 0)
				((ChildWindowData*)Controller::GetInstance()->GetActiveWndData())->Copy(hWnd);
			break;

		case ID_EDIT_PASTE:
			if (n_draw > 0)
				((ChildWindowData*)Controller::GetInstance()->GetActiveWndData())->Paste(hWnd);
			break;

		case ID_EDIT_DELETE:
			if (n_draw > 0)
				((ChildWindowData*)Controller::GetInstance()->GetActiveWndData())->Delete(hWnd);
			break;

		case ID_DRAW_COLOR: 
			DrawChooseColor(hWnd);
			break;

		case ID_DRAW_FONT: 
			DrawChooseFont(hWnd);
			break;

		case ID_DRAW_LINE:
			if (n_draw > 0) {
				((ChildWindowData*)Controller::GetInstance()->GetActiveWndData())->activeTool = ToolType::Line;
				UpdateStateMenu(hWnd, wmId);
			}
			break;

		case ID_DRAW_RECTANGLE:
			if (n_draw > 0) {
				((ChildWindowData*)Controller::GetInstance()->GetActiveWndData())->activeTool = ToolType::Rectangle;
				UpdateStateMenu(hWnd, wmId);
			}
			break;

		case ID_DRAW_ELLIPSE:
			if (n_draw > 0) {
				((ChildWindowData*)Controller::GetInstance()->GetActiveWndData())->activeTool = ToolType::Ellipse;
				UpdateStateMenu(hWnd, wmId);
			}
			break;

		case ID_DRAW_TEXT:
			if (n_draw > 0)
				((ChildWindowData*)Controller::GetInstance()->GetActiveWndData())->activeTool = ToolType::Text;
			UpdateStateMenu(hWnd, wmId);
			break;

		case ID_DRAW_SELECTOBJECT:
			if (n_draw > 0) {
				((ChildWindowData*)Controller::GetInstance()->GetActiveWndData())->activeTool = ToolType::Select_Object;
				UpdateStateMenu(hWnd, wmId);
			}
			break;

		case ID_WINDOW_TILE:
			SendMessage(hWndDrawClient, WM_MDITILE, MDITILE_VERTICAL, 0);
			break;

		case ID_WINDOW_CASCADE:
			SendMessage(hWndDrawClient, WM_MDICASCADE, MDITILE_ZORDER | MDITILE_SKIPDISABLED, 0);
			break;

		case ID_WINDOW_CLOSEALL:
			EnumChildWindows(hWndDrawClient, (WNDENUMPROC)MDICloseProc, 0L);
			break;

		default:
			return DefFrameProc(hWnd, hWndDrawClient, message, wParam, lParam);
		}
	}
	return DefFrameProc(hWnd, hWndDrawClient, message, wParam, lParam);
	break;

	case WM_SIZE:
	{
		RECT rc;
		RECT rcTool;
		GetClientRect(hWnd, &rc);
		GetWindowRect(hToolBarWnd, &rcTool);
		int toolBarHeight = rcTool.bottom - rcTool.top;
		SetWindowPos(hToolBarWnd, 0, 0, 0, rc.right, toolBarHeight, 0);
		MoveWindow(hWndDrawClient, 0, toolBarHeight, rc.right, rc.bottom, TRUE);
		return 0;
	}
	return DefFrameProc(hWnd, hWndDrawClient, message, wParam, lParam);
	break;

	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		
		EndPaint(hWnd, &ps);
	}
	break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;

	default:
		return DefFrameProc(hWnd, hWndDrawClient, message, wParam, lParam);
	}
	return DefFrameProc(hWnd, hWndDrawClient, message, wParam, lParam);
}

void CreateDrawWin() {
	MDICREATESTRUCT mdiCreate;
	TCHAR draw_title[50];
	n_draw++;
	_stprintf_p(draw_title, 50, L"Noname-%d.drx", n_draw);
	mdiCreate.szClass = L"MDI_DRAW_CHILD";
	mdiCreate.szTitle = (LPCWSTR)draw_title;
	mdiCreate.hOwner = hInst;
	mdiCreate.x = CW_USEDEFAULT;
	mdiCreate.y = CW_USEDEFAULT;
	mdiCreate.cx = CW_USEDEFAULT;
	mdiCreate.cy = CW_USEDEFAULT;
	mdiCreate.style = 0;
	mdiCreate.lParam = NULL;

	SendMessage(hWndDrawClient, WM_MDICREATE, 0, (LPARAM)(LPMDICREATESTRUCT)&mdiCreate);
}

void CreateDrawWinFromFile(LPCWSTR draw_title) {
	MDICREATESTRUCT mdiCreate;
	mdiCreate.szClass = L"MDI_DRAW_CHILD";
	mdiCreate.szTitle = (LPCWSTR)draw_title;
	mdiCreate.hOwner = hInst;
	mdiCreate.x = CW_USEDEFAULT;
	mdiCreate.y = CW_USEDEFAULT;
	mdiCreate.cx = CW_USEDEFAULT;
	mdiCreate.cy = CW_USEDEFAULT;
	mdiCreate.style = 0;
	mdiCreate.lParam = NULL;
	SendMessage(hWndDrawClient, WM_MDICREATE, 0, (LPARAM)(LPMDICREATESTRUCT)&mdiCreate);
}

void UpdateStateMenu(HWND hWnd, int newMenuItem) {
	HMENU hPopup = GetSubMenu(GetMenu(hWnd), 1);
	CheckMenuItem(hPopup, nCurrentSelectedMenuItem, MF_UNCHECKED | MF_BYCOMMAND);
	CheckMenuItem(hPopup, newMenuItem, MF_CHECKED | MF_BYCOMMAND);
	nCurrentSelectedMenuItem = newMenuItem;
}

void DrawChooseColor(HWND hWnd) {
	CHOOSECOLOR  cc;
	COLORREF  acrCustClr[16];  
	DWORD  rgbCurrent = RGB(255, 0, 0);  
										
	ZeroMemory(&cc, sizeof(CHOOSECOLOR));
	cc.lStructSize = sizeof(CHOOSECOLOR);
	cc.hwndOwner = hWnd;  
	cc.lpCustColors = (LPDWORD)acrCustClr;
	cc.rgbResult = rgbCurrent; 
	cc.Flags = CC_FULLOPEN | CC_RGBINIT;
	if (ChooseColor(&cc)) {  
		HBRUSH  hbrush;
		hbrush = CreateSolidBrush(cc.rgbResult);
		rgbCurrent = cc.rgbResult;
		Controller::GetInstance()->GetActiveWndData()->GetColor(rgbCurrent);
	}
}

void DrawChooseFont(HWND hWnd) {
	CHOOSEFONT  cf;
	LOGFONT  lf;               
	HFONT  hfNew, hfOld;
	ZeroMemory(&cf, sizeof(CHOOSEFONT));
	cf.lStructSize = sizeof(CHOOSEFONT);
	cf.hwndOwner = hWnd; 
	cf.lpLogFont = &lf;
	cf.rgbColors = RGB(255, 0, 0);
	cf.Flags = CF_SCREENFONTS | CF_EFFECTS;
	if (ChooseFont(&cf)) {       
		hfNew = CreateFontIndirect(cf.lpLogFont);
		Controller::GetInstance()->GetActiveWndData()->GetFont(lf);
	}
}

void DrawSaveFile(HWND hWnd) {
	OPENFILENAME  ofn;
	TCHAR  szFile[256];
	TCHAR  szFilter[]  =  L"Draw File(*.drw)\0*.drw\0";
	TCHAR draw_title[50];
	_stprintf_p(draw_title, 50, L"Noname-%d.drx", n_draw);
	szFile[0]  =  '\0';
	ZeroMemory(&ofn,  sizeof(OPENFILENAME));  
	ofn.lStructSize  =  sizeof(OPENFILENAME);
	ofn.hwndOwner =  hWnd; 
	ofn.lpstrFilter =  szFilter;
	ofn.nFilterIndex  =  1;
	ofn.lpstrFile = draw_title;
	ofn.lpstrFileTitle = draw_title;
	ofn.nMaxFile  =  sizeof(szFile);
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_OVERWRITEPROMPT;
	if (GetSaveFileName(&ofn)) {
		Controller::GetInstance()->GetActiveWndData()->GetFilename(ofn.lpstrFile);
	}
}

void DrawOpenFile(HWND hWnd) {
	OPENFILENAME  ofn;
	TCHAR  szFile[256];
	TCHAR  szFilter[] = L"Draw File(*.drw)\0*.drw\0";
	TCHAR  szFileTitle[256];
	szFileTitle[0] = '\0';
	szFile[0] = '\0';
	ZeroMemory(&ofn, sizeof(OPENFILENAME));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = hWnd;
	ofn.lpstrFilter = szFilter;
	ofn.nFilterIndex = 1;
	ofn.lpstrFile = szFile;
	ofn.lpstrFileTitle = szFileTitle;
	ofn.nMaxFile = sizeof(szFile);
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_OVERWRITEPROMPT;
	if (GetOpenFileName(&ofn)) {
		//Spilt path and filename. Ex: C:\\Dekstop\\123.drw => 123.drw
		wstring appendedfilename;
		appendedfilename += ofn.lpstrFile;
		LPCWSTR tmp = appendedfilename.c_str();
		int count = 0;
		for (int i = wcslen(tmp); i >= 4; i--) {
			if (tmp[i] == '\\') {
				count = i + 1;
				break;
			}
		}
		wstring fileTitle;
		for (int i = count; i <= wcslen(tmp); i++) {
			fileTitle += tmp[i];
		}
		LPCWSTR tmp2 = fileTitle.c_str();
		//Create window with filename
		CreateDrawWinFromFile(tmp2);
		//
		Controller::GetInstance()->GetActiveWndData()->GetFilename(ofn.lpstrFile);
		Controller::GetInstance()->GetActiveWndData()->OpenFile();
	}
}

void doCreate_ToolBar(HWND hWnd)
{
	// loading Common Control DLL
	InitCommonControls();

	TBBUTTON tbButtons[] =
	{
		// Zero-based Bitmap image, ID of command, Button state, Button style, 
		// ...App data, Zero-based string (Button's label)
		{ STD_FILENEW,	ID_FILE_NEW, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ STD_FILEOPEN,	ID_FILE_OPEN, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ STD_FILESAVE,	ID_FILE_SAVE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	};

	// create a toolbar
	hToolBarWnd = CreateToolbarEx(hWnd,
		WS_CHILD | WS_VISIBLE | CCS_ADJUSTABLE | TBSTYLE_TOOLTIPS,
		ID_TOOLBAR,
		sizeof(tbButtons) / sizeof(TBBUTTON),
		HINST_COMMCTRL,
		0,
		tbButtons,
		sizeof(tbButtons) / sizeof(TBBUTTON),
		BUTTON_WIDTH,
		BUTTON_HEIGHT,
		IMAGE_WIDTH,
		IMAGE_HEIGHT,
		sizeof(TBBUTTON));
}

void doToolBar_AddUserButton()
{
	// define new buttons
	TBBUTTON tbButtons[] =
	{
		{ 0, 0,	TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0 },
		{ 0, ID_DRAW_LINE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 1, ID_DRAW_RECTANGLE,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 2, ID_DRAW_ELLIPSE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 3, ID_DRAW_TEXT, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 4, ID_DRAW_SELECTOBJECT, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 5, 0,	TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0 },

	};

	TBBUTTON tbButtons2[] =
	{
		{ 3, 0,	TBSTATE_ENABLED, TBSTYLE_SEP, 0, 0 },
		{ 4, ID_EDIT_COPY, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 5, ID_EDIT_PASTE,	TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 6, ID_EDIT_CUT, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
		{ 7, ID_EDIT_DELETE, TBSTATE_ENABLED, TBSTYLE_BUTTON, 0, 0 },
	};

	// structure contains the bitmap of user defined buttons. It contains 2 icons
	TBADDBITMAP	tbBitmap = { hInst, IDB_BITMAP1 };
	TBADDBITMAP tbBitmap2 = { hInst, IDB_BITMAP3 };

	// Add bitmap to Image-list of ToolBar
	int idx = SendMessage(hToolBarWnd, TB_ADDBITMAP, (WPARAM) sizeof(tbBitmap) / sizeof(TBADDBITMAP),
		(LPARAM)(LPTBADDBITMAP)&tbBitmap);

	int idx2 = SendMessage(hToolBarWnd, TB_ADDBITMAP, (WPARAM) sizeof(tbBitmap2) / sizeof(TBADDBITMAP),
		(LPARAM)(LPTBADDBITMAP)&tbBitmap2);

	// identify the bitmap index of each button
	for (int i = 1; i <= 5; i++) {
		tbButtons[i].iBitmap += idx;
	}

	for (int i = 1; i <= 4; i++) {
		tbButtons2[i].iBitmap += idx2;
	}

	// add buttons to toolbar
	SendMessage(hToolBarWnd, TB_ADDBUTTONS, (WPARAM) sizeof(tbButtons) / sizeof(TBBUTTON),
		(LPARAM)(LPTBBUTTON)&tbButtons);
	SendMessage(hToolBarWnd, TB_ADDBUTTONS, (WPARAM) sizeof(tbButtons2) / sizeof(TBBUTTON),
		(LPARAM)(LPTBBUTTON)&tbButtons2);
}
