//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by P1.rc
//
#define IDC_MYICON                      2
#define IDD_P1_DIALOG                   102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_P1                          107
#define IDI_SMALL                       108
#define IDC_P1                          109
#define IDR_MAINFRAME                   128
#define IDB_BITMAP1                     131
#define IDD_DIALOG1                     132
#define IDB_BITMAP2                     132
#define IDB_BITMAP3                     135
#define IDC_EDIT1                       1000
#define ID_FILE_OPEN                    32771
#define ID_FILE_SAVE                    32772
#define ID_FILE_EXIT                    32773
#define ID_DRAW_COLOR                   32774
#define ID_DRAW_FONT                    32775
#define ID_DRAW_LINE                    32776
#define ID_DRAW_RECTANGLE               32777
#define ID_DRAW_ELLIPSE                 32778
#define ID_DRAW_TEXT                    32779
#define ID_DRAW_SELECTOBJECT            32780
#define ID_WINDOW_TILE                  32781
#define ID_WINDOW_CASCADE               32782
#define ID_WINDOW_CLOSEALL              32783
#define ID_FILE_NEW                     32784
#define ID_FILE_CUT                     32791
#define ID_FILE_COPY                    32792
#define ID_FILE_PASTE                   32793
#define ID_FILE_DELETE                  32794
#define ID_EDIT_CUT                     32800
#define ID_EDIT_COPY                    32801
#define ID_EDIT_PASTE                   32802
#define ID_EDIT_DELETE                  32803
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        136
#define _APS_NEXT_COMMAND_VALUE         32804
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
