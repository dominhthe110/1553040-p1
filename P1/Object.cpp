#include "stdafx.h"


//Line
Line_O::Line_O(int _left, int _top, int _right, int _bottom, COLORREF _color) {
	type = ToolType::Line;
	left = _left;
	top = _top;
	right = _right;
	bottom = _bottom;
	color = _color;
}

void Line_O::Draw(HDC hdc) {
	HPEN hPen = CreatePen(PS_SOLID, 3, color);

	SelectObject(hdc, hPen);
	MoveToEx(hdc, left, top, NULL);
	LineTo(hdc, right, bottom);
	DeleteObject(hPen);
}

void Line_O::MoveHandleTo(int x, int y) {
	right = x;
	bottom = y;
}

bool Line_O::IsSelected(int x, int y) {
	int a, b, func;

	a = right - left;
	b = bottom - top;
	func = b * (x - left) - a * (y - top);

	if (func >= 0 && func <= 200 || func <= 0 && func >= -200)
		return true;
	return false;
}

void Line_O::Selected(HWND hWnd) {
	HDC hdc;
	HPEN pen;
	hdc = GetDC(hWnd);
	pen = CreatePen(PS_SOLID, 1, RGB(255, 0, 0));
	SelectObject(hdc, pen);
	Ellipse(hdc, left - 5, top - 5, left + 5, top + 5);
	Ellipse(hdc, right - 5,bottom - 5, right + 5, bottom + 5);
	DeleteObject(hdc);
	DeleteObject(pen);
}

//Rectangle
Rectangle_O::Rectangle_O(int _left, int _top, int _right, int _bottom, COLORREF _color) {
	type = ToolType::Rectangle;
	left = _left;
	top = _top;
	right = _right;
	bottom = _bottom;
	color = _color;
}

void Rectangle_O::Draw(HDC hdc) {
	HPEN hPen = CreatePen(PS_SOLID, 3, color);

	SelectObject(hdc, hPen);
	Rectangle(hdc, left, top, right, bottom);
	DeleteObject(hPen);
}

void Rectangle_O::MoveHandleTo(int x, int y) {
	right = x;
	bottom = y;
}

bool Rectangle_O::IsSelected(int x, int y) {
	if (bottom < top)
	{
		if ((x >= left
			&& x <= right
			&& y <= top
			&& y >= bottom) || x <= left
			&& x >= right
			&& y <= top
			&& y >= bottom)
			return true;
	}
	else
	{
		if ((x >= left
			&& x <= right
			&& y >= top
			&& y <= bottom) || x <= left
			&& x >= right
			&& y >= top
			&& y <= bottom)
			return true;
	}
	return false;
}

void Rectangle_O::Selected(HWND hWnd) {
	HDC hdc;
	HPEN pen;
	hdc = GetDC(hWnd);
	pen = CreatePen(PS_SOLID, 1, RGB(255, 0, 0));
	SelectObject(hdc, pen);
	Ellipse(hdc, left - 5, top - 5, left + 5, top + 5);
	Ellipse(hdc, left - 5, bottom - 5, left + 5, bottom + 5);
	Ellipse(hdc, right - 5, top - 5, right + 5, top + 5);
	Ellipse(hdc, right - 5, bottom - 5, right + 5, bottom + 5);
	DeleteObject(hdc);
	DeleteObject(pen);
}

//Ellipse
Ellipse_O::Ellipse_O(int _left, int _top, int _right, int _bottom, COLORREF _color) {
	type = ToolType::Ellipse;
	left = _left;
	top = _top;
	right = _right;
	bottom = _bottom;
	color = _color;
}

void Ellipse_O::Draw(HDC hdc) {
	HPEN hPen = CreatePen(PS_SOLID, 3, color);

	SelectObject(hdc, hPen);
	Ellipse(hdc, left, top, right, bottom);
	DeleteObject(hPen);
}

void Ellipse_O::MoveHandleTo(int x, int y) {
	right = x;
	bottom = y;
}

bool Ellipse_O::IsSelected(int x, int y) {
	int a, b, func;
	int r;

	a = abs(left + ((right - left) / 2));
	b = abs(top + (bottom - top) / 2);
	r = abs((right - left) / 2);
	func = pow(x - a, 2) + pow(y - b, 2);
	if (func <= pow(r, 2))
		return true;
	return false;
}

void Ellipse_O::Selected(HWND hWnd) {
	HDC hdc;
	HPEN pen;
	hdc = GetDC(hWnd);
	pen = CreatePen(PS_SOLID, 1, RGB(255, 0, 0));
	SelectObject(hdc, pen);
	Ellipse(hdc, abs(left + ((right - left) / 2)) - 5, top - 5, abs(left + ((right - left) / 2)) + 5, top + 5);
	Ellipse(hdc, abs(left + ((right - left) / 2)) - 5, bottom - 5, abs(left + ((right - left) / 2)) + 5, bottom + 5);
	Ellipse(hdc, left - 5, abs(top + (bottom - top) / 2) - 5, left + 5, abs(top + (bottom - top) / 2) + 5);
	Ellipse(hdc, right - 5, abs(top + (bottom - top) / 2) - 5, right + 5, abs(top + (bottom - top) / 2) + 5);
	DeleteObject(hdc);
	DeleteObject(pen);
}

//Text
Text_O::Text_O(int _left, int _top, int _right, int _bottom, COLORREF _color, LOGFONT _font) {
	type = ToolType::Text;
	left = _left;
	top = _top;
	right = _right;
	bottom = _bottom;
	color = _color;
	font = _font;
}

void Text_O::Draw(HDC hdc) {
	HFONT hfNew = CreateFontIndirect(&font);
	SelectObject(hdc, hfNew);
	SetTextColor(hdc, color);
	TextOut(hdc, left, top, str, wcslen(str));
	//Update size of rectangle contains Text
	if (top == bottom && left == right) {
		SIZE size;
		GetTextExtentPoint32(hdc, str, wcslen(str), &size);
		right = right + size.cx;
		bottom = bottom + size.cy;
	}
}

void Text_O::MoveHandleTo(int x, int y) {
	
}

bool Text_O::IsSelected(int x, int y) {
	int a, b, func;
	int r;
	
	if ((x >= left 
		&& x <= right
		&& y >= top 
		&& y <= bottom) 
		|| (x <= left 
		&& x >= right 
		&& y >= top 
		&& y <= bottom))
		return true;
	return false;
}

void Text_O::Selected(HWND hWnd) {
	HDC hdc;
	HPEN pen;
	hdc = GetDC(hWnd);
	pen = CreatePen(PS_SOLID, 1, RGB(255, 0, 0));
	SelectObject(hdc, pen);
	Ellipse(hdc, left - 3, top - 3, left + 3, top + 3);
	Ellipse(hdc, left - 3, bottom - 3, left + 3, bottom + 3);
	Ellipse(hdc, right - 3, top - 3, right + 3, top + 3);
	Ellipse(hdc, right - 3, bottom - 3, right + 3, bottom + 3);
	DeleteObject(hdc);
	DeleteObject(pen);
}
